console.log("Discussion s25 Intro To JSON");

// /* 

//         JSON Object
//             -
//             JSON stands
//         for JavaScript Object Notation
//             -
//             JSON is also used
//         for other prog.languages hence the name javascript


//         Syntax:

//             {
//                 "PropertA": "valueA",
//                 "PropertB": "valueB"


//             }


// */

// // JSON OBJECT

// // {

// // "city" : "Quezon City",
// // "province" : "Metro Manila",
// // "country" : "Philippines" 


// // }


// //JSON Array
// "cities" = [

//     {"city" : "Quezon City" , "province" : "Metro Manila", "country" : "Philippines" },
//     {"city" : "Manila City" , "province" : "Metro Manila", "country" : "Philippines" },
//     {"city" : "Makati City" , "province" : "Metro Manila", "country" : "Philippines" }

// ]

//JSON Methods
/*  
        The JSON Object contains methods for parsing/pairing and converting
        data into stringified JSON
*/

// Converting Data into Stringified JSON

let batchArr = [
            {batchname: "batch198"},
           { batchname: "batch197"
        }

]
console.log(batchArr);

console.log("");

//The stringify method is used to convert JS OBjects...
//We are doing...

console.log("Result fron strigify method");
console.log(JSON.stringify(batchArr));

console.log("");
let userProfile = JSON.stringify({

        name:"John",
        age: 31,
        address:{
                city:"Pasig",
                region:"NCR",
                country:"Philippines"
        }



})

console.log("Result fron strigify method (Profile)");
console.log(userProfile);

console.log("");

//User Details

/* let firstName = prompt("What is your first name? ")
let lastName = prompt("What is your last name? ")
let age = prompt("What is your first age? ")
let address = {
           city: prompt("Which city you live in?"),
           country : prompt("Which city you live in that is in a country?")
}

let userData = JSON.stringify({
    firstName : firstName,
    lastName: lastName,
    age : age,
    address : address

})

console.log(userData) */

console.log("");

//Convert stringified JSON into JS Objects
//JSON.parse()

let batchesJSON = `[
    {"batchname": "batch198"},
    { "batchname": "batch197"
 }




]`
console.log("Result fron parse method:")
console.log(JSON.parse(batchesJSON));

console.log("");
let strinifiedObject = `{

        "name" : "Ivy",
        "age" : "18",
        "address" : {
            "city":"Pasig",
            "region":"NCR",
            "country":"Philippines"
    }


}`

console.log(strinifiedObject)
console.log("Result from parse method (object)")
console.log(JSON.parse(strinifiedObject))